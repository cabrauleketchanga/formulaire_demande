const withLess = require('next-with-less');
/** @type {import('next').NextConfig} */
module.exports = withLess({
  lessLoaderOptions: {
    lessOptions: {

      modifyVars: {
        // Add variables here
        'border-radius-base': '50px',
        '@primary-color': "#04f",
        '@primary-color': "#000",
      },
    },
  },
  compiler: {
    styledComponents: true,
  },
});