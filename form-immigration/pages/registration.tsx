
import React, { useEffect, useState } from 'react';
import { Button, Checkbox, Form, Input, Select } from 'antd';
import FormOne from './components/form_one';
import FormTwo from './components/form_two';


enum Step {
    step1,
    step2,
}
export default function Registration() {
    const [step, setStep] = useState(Step.step1);
    const [form, setForm] = useState({
        lien: 'Parent',
        categorie: 'visiteur',
        name: "",
        niveau_etude: "Baccalauréat",
        nom_guarant: "",
        jour_naissance: "01",
        mois_naissance: "Janvier",
        annee_naissance: "1990",
    })
    const handleForm = () => {
        console.log(form);
    }
    return (
        <>
            <main className=" transition-all duration-700  h-screen w-screen  lg:p-[4rem] bg-background ">
                <div className="space-y-10 p-4">
                    <h3 className="font-semiBold flex justify-center items-start text-black text-md md:text-[22px] lg:text-[32px]">Demande de contact pour évaluation</h3>

                    {step == Step.step1 ? <FormOne form={form} setForm={setForm} callBack={() => setStep(Step.step2)} /> :
                        <FormTwo form={form} setForm={setForm} callBack={() => setStep(Step.step1)} callNext={() => { handleForm() }} />
                    }
                </div>
            </main>
        </>
    );
}
