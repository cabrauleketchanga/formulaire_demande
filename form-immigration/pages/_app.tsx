import '@/styles/globals.css'
import { StyleProvider } from '@ant-design/cssinjs';
import type { AppProps } from 'next/app';
import '../public/antd.min.css';
import '../styles/globals.css'; // 
export default function App({ Component, pageProps }: AppProps) {
  return <StyleProvider hashPriority="high">
    <Component {...pageProps} />
  </StyleProvider>


}
