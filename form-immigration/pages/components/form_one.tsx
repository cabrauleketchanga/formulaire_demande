import React, { useEffect } from 'react';
import { Form, Input, Select } from 'antd';

interface FieldType {
    name?: string;
    jour_naissance?: string;
    mois_naissance?: string;
    annee_naissance?: string;
    niveau_etude?: string;
    occupation?: string;
    experience?: string;
    lieu_naissance: string;
};
interface Props {
    callBack: Function,
    setForm: any,
    form: any,
}
export default function FormOne(props: Props) {
    const [formField] = Form.useForm();
    console.log(props.form);
    const handleSubmit = () => {
        formField.validateFields().then((values) => {
            props.callBack();
        }).catch((err) => {
            console.log(err);
        });
    }

    useEffect(() => {
        formField.setFieldsValue({
            name: props?.form?.name ?? '',
            jour_naissance: props.form?.jour_naissance,
            mois_naissance: props.form?.mois_naissance,
            annee_naissance: props.form?.annee_naissance,
            niveau_etude: props.form?.niveau_etude,
            occupation: props.form?.occupation,
            experience: props.form?.experience,
            lieu_naissance: props.form?.lieu_naissance,
        });
    }, [])
    return (
        <div className="min-h-[700px] transition-all duration-700 rounded-lg flex justify-center xl:justify-end p-3  md:p-5  items-start w-full bg-no-repeat bg-cover bg-center bg-[url('/images/background_1.png')]">
            <div className="bg-background flex flex-col justify-start items-start w-[100%] md:w-[70%] lg:w-[60%] xl:w-[40%] rounded-[17px] p-3 md:p-5 ">
                <div className="flex pb-10 space-x-2 justify-start items-center">
                    <div className="flex justify-center items-center h-[43px] w-[43px] text-background font-medium  rounded-[43px] bg-greenColor">1</div>
                    <h4 className="font-medium  text-greenColor text-[16px] md:text-[28px]">Informations générales</h4>
                </div>

                <Form
                    form={formField}
                    name="wrap"
                >

                    <Form.Item<FieldType> name="name" rules={[{ required: true, message: 'Entrez votre nom et prenom' }]}>
                        <div>
                            <h4 className='text-mouseGray text-[14px] pb-1 font-regular'>Nom et prenom</h4>
                            <Input defaultValue={props.form?.name ?? ''} onChange={(v) => props.setForm({ ...props.form, name: v.target.value })} />
                        </div>
                    </Form.Item>

                    <div className='block md:flex justify-start items-start space-x-0 md:space-x-2'>
                        <div className='w-full md:w-[70%]'>
                            <h4 className='text-mouseGray text-[14px] pb-1 font-regular'>Date de naissance</h4>
                            <div className='flex space-x-2 justify-between items-center'>
                                <Select
                                    style={{ width: "100%" }}
                                    defaultValue={props.form?.jour_naissance ?? '01'}
                                    onChange={(v) => props.setForm({ ...props.form, jour_naissance: v })}
                                    options={[
                                        { value: '01', label: '01' },
                                        { value: '02', label: '02' },
                                        { value: '03', label: '03' },
                                        { value: '04', label: '04' },
                                        { value: '05', label: '05' },
                                        { value: '06', label: '06' },
                                        { value: '07', label: '07' },
                                        { value: '08', label: '08' },
                                        { value: '09', label: '09' },
                                        { value: '130', label: '10' },
                                        { value: '11', label: '11' }, { value: '12', label: '12' }, { value: '13', label: '13' }, { value: '14', label: '14' }, { value: '15', label: '15' }, { value: '16', label: '16' }, { value: '17', label: '17' }, { value: '18', label: '18' }, { value: '19', label: '19' }, { value: '20', label: '20' }, { value: '21', label: '21' }, { value: '22', label: '22' }, { value: '23', label: '23' }, { value: '24', label: '24' }, { value: '25', label: '25' }, { value: '26', label: '26' }, { value: '27', label: '27' }, { value: '28', label: '28' }, { value: '29', label: '29' }, { value: '30', label: '30' }, { value: '31', label: '31' },
                                    ]}
                                />
                                <Select
                                    defaultValue={props.form?.mois_naissance ?? 'Janvier'}
                                    onChange={(v) => props.setForm({ ...props.form, mois_naissance: v })}

                                    options={[{ value: 'Janvier', label: 'Janvier' }, { value: 'Fevriér', label: 'Fevriér' }, { value: 'Mars', label: 'Mars' }, { value: 'Avril', label: 'Avril' }, { value: 'Mai', label: 'Mai' }, { value: 'Juin', label: 'Juin' }, { value: 'Juillet', label: 'Juillet' }, { value: 'Aout', label: 'Aout' }, { value: 'Septembre', label: 'Septembre' }, { value: 'Octobre', label: 'Octobre' }, { value: 'Novembre', label: 'Novembre' }, { value: 'Decembre', label: 'Decembre' },]}
                                />
                                <Select
                                    defaultValue={props.form?.annee_naissance ?? '1990'}
                                    style={{ width: "150px" }}
                                    onChange={(v) => props.setForm({ ...props.form, annee_naissance: v })}

                                    options={[
                                        { value: '1990', label: '1990' },
                                        { value: '1991', label: '1991' },
                                        { value: '1992', label: '1992' }, { value: '1993', label: '1993' }, { value: '1994', label: '1994' }, { value: '1995', label: '1995' }, { value: '1996', label: '1996' }, { value: '1997', label: '1997' }, { value: '1998', label: '1998' }, { value: '1999', label: '1999' }, { value: '2000', label: '2000' }, { value: '2001', label: '2001' }, { value: '2002', label: '2002' },
                                    ]}
                                />
                            </div>
                        </div>
                        <Form.Item<FieldType> name="lieu_naissance" rules={[{ required: true, message: 'Lieu vide' }]}>
                            <div className=''>

                                <h4 className='text-mouseGray text-[14px] pb-1 font-regular'>Lieu</h4>
                                <Input defaultValue={props.form?.lieu_naissance ?? ''}
                                    onChange={(v) => props.setForm({ ...props.form, lieu_naissance: v.target.value })} />
                            </div>
                        </Form.Item>

                    </div>

                    <Select className='mb-4' style={{ width: "100%" }}
                        onChange={(v) => props.setForm({ ...props.form, niveau_etude: v })}
                        defaultValue={props.form?.niveau_etude ?? 'Baccalaureat'}
                        options={[
                            { value: 'Certificat d’études primaires', label: 'Certificat d’études primaires' },
                            { value: 'Brevet d’étude du premier cycle (BEPC) ou équivalent', label: 'Brevet d’étude du premier cycle (BEPC) ou équivalent' },
                            { value: 'Baccalaureat', label: 'Baccalaureat' },
                            { value: "Brevet de techicien superieur, DUT ou BAC+2", label: "Brevet de techicien superieur, DUT ou BAC+2" },
                            { value: "licence professionnelle ou BAC+3", label: "licence professionnelle ou BAC+3" },
                            { value: "Master Professionnel ou BAC+5", label: "Master Professionnel ou BAC+5" }
                        ]}
                    />
                    <div className='block mt-5  md:flex  space-x-0 md:space-x-2  justify-start items-center'>
                        <Form.Item<FieldType> className='w-full md:w-[60%]' name="occupation" rules={[{ required: true, message: "Entrez votre occupation" }]}>
                            <div>
                                <h4 className='text-mouseGray text-[14px] pb-1 font-regular'>Occupation</h4>
                                <Input defaultValue={props.form?.occupation ?? ''}
                                    onChange={(v) => props.setForm({ ...props.form, occupation: v.target.value })} />
                            </div>
                        </Form.Item>
                        <Form.Item<FieldType> className='w-full md:w-[40%]' name="experience" rules={[{ required: true, message: 'Entrez le nombre d\'année ' }]}>
                            <div>
                                <h4 className='text-mouseGray text-[14px] pb-1 font-regular'>Années d’expérience</h4>
                                <Input defaultValue={props.form?.experience ?? ''}
                                    onChange={(v) => props.setForm({ ...props.form, experience: v.target.value })} />
                            </div>
                        </Form.Item>
                    </div>
                    <Form.Item className='pt-[5rem]' >
                        <button type='submit' onClick={() => { handleSubmit(), console.log(formField.isFieldsValidating) }} className="bg-yellowColor flex justify-center items-center px-10 h-[48px] font-medium text-[14px] text-background rounded-[12px] mt-10">Suivant</button>
                    </Form.Item>

                </Form>

            </div>
        </div >
    );
}