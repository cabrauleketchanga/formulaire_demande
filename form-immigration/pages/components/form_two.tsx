import React, { useEffect, useState } from 'react';
import { Form, Input, Select } from 'antd';
import CheckBox from './check_box';
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';
interface Props {
    callBack: Function,
    callNext: Function,
    setForm: any,
    form: any,
}
interface FieldType {
    nom_guarant?: string;
    personnes?: string;
    lien: string;
};

export default function FormTwo(props: Props) {
    const router = useRouter();
    console.log(props.form);

    const [cat, setCat] = useState('visiteur');
    const [form] = Form.useForm();
    const [isSend, setIsSend] = useState(false);

    const handleSubmit = () => {
        setIsSend(true);
        console.log(props.form.niveau_etude);
        form.validateFields().then((values) => {
            props.callNext()
        }).catch((err) => {
            console.log(err);
        });
    }
    useEffect(() => {
        form.setFieldsValue({
            nom_guarant: props.form?.nom_guarant ?? '',
            personnes: props.form?.personnes ?? 'Je viens seul(e)',
            lien: props.form?.lien ?? 'Parents',
        });
    }, [])
    return (
        <>
            <div style={{ backgroundSize: 'cover', backgroundRepeat: 'no-repeat', backgroundImage: cat == 'visiteur' ? `url("/images/background_2.png")` : cat == 'travailleur' ? `url("/images/background_3.png")` : `url("/images/background_4.png")` }} className="min-h-[700px] transition-all duration-700  rounded-lg   w-[100%] ">
                {
                    isSend == false ? <div style={{ width: '100%' }} className=' flex justify-end p-5  items-start '>
                        <div className="bg-background    flex flex-col justify-start items-start  w-[100%] md:w-[70%] lg:w-[60%] xl:w-[40%] rounded-[17px] p-5 ">
                            <div className="flex pb-10 space-x-2 justify-start items-start">
                                <div className="flex justify-center items-center h-[43px] w-[43px] text-background font-medium  rounded-[43px] bg-greenColor">2</div>
                                <h4 className="font-medium  text-greenColor text-[16px] md:text-[28px]">Catégorie d’immigration</h4>
                            </div>

                            <Form style={{ width: '100%' }}
                                form={form}
                                name="wra"

                            >
                                <div style={{ width: '100%' }}>
                                    <h4 className='text-mouseGray text-[14px] pb-1 font-regular'>Choisissez votre catégorie d’immigration</h4>
                                    <div className=' block md:flex justify-start items-center md:space-x-5'>
                                        <CheckBox onChange={() => { props.setForm({ ...props.form, categorie: 'visiteur' }), setCat('visiteur') }} title='Visiteur' status={cat == 'visiteur' ? true : false} />
                                        <CheckBox onChange={() => { props.setForm({ ...props.form, categorie: 'travailleur' }), setCat('travailleur') }} title='Travailleur' status={cat == 'travailleur' ? true : false} />
                                        <CheckBox onChange={() => { props.setForm({ ...props.form, categorie: 'etudiant' }), setCat('etudiant') }} title='Étudiant' status={cat == 'etudiant' ? true : false} />
                                    </div>
                                    <div className='flex w-full h-[1px] mt-3 mb-5 bg-[#D9D9D9]' />
                                </div>
                                {
                                    cat != 'travailleur' && (<Form.Item<FieldType> name="nom_guarant" rules={[{ required: true, message: 'Entrez le nom du guarant' }]}>
                                        <div>
                                            <h4 className='text-mouseGray text-[14px] pb-1 font-regular'>Nom et prénom du garant financier</h4>
                                            <Input defaultValue={props.form?.nom_guarant ?? ''} onChange={(v) => props.setForm({ ...props.form, nom_guarant: v.target.value })} />
                                        </div>
                                    </Form.Item>)
                                }

                                {cat != 'travailleur' && (<Form.Item>
                                    <div style={{ width: '100%' }}>
                                        <h4 className='text-mouseGray text-[14px] pb-1 font-regular'>Lien avec le garant</h4>
                                        <Select style={{ width: '100%' }}
                                            defaultValue={props.form?.lien ?? 'Parent'} onChange={(v) => props.setForm({ ...props.form, lien: v })}
                                            options={[
                                                { value: 'Parent', label: 'Parent' },
                                                { value: 'Tuteur(trice)', label: 'Tuteur(trice)' },
                                                { value: 'Ami(e)', label: 'Ami(e)' },
                                                { value: 'Enfant', label: 'Enfant' },
                                                { value: 'Époux(se)', label: 'Époux(se)' },
                                            ]}
                                        />
                                    </div>

                                </Form.Item>)
                                }

                                {cat == 'travailleur' ? (<h4 className='text-mouseGray text-[14px] pb-1 font-regular'>Definir la taille de la famille</h4>
                                ) : cat == 'visiteur' ? (<h4 className='text-mouseGray text-[14px] pb-1 font-regular'>Nombre de personnes voyageant avec vous</h4>
                                ) : <></>}
                                {cat != 'etudiant' && (<Select style={{ width: '100%' }}
                                    defaultValue={props.form?.personnes ?? 'Je viens seul(e)'} onChange={(v) => props.setForm({ ...props.form, personnes: v })}
                                    options={[
                                        { value: 'Je viens seul(e)"', label: 'Je viens seul(e)"' },
                                        { value: '01', label: '  01' },
                                        { value: '02', label: '02' }, { value: '03', label: '03' }, { value: '04', label: '04' },
                                    ]}
                                />)
                                }


                                <div className='bloc  md:flex pt-[2rem] md:pt-[5rem] justify-start md:space-x-2 items-center'>
                                    <Form.Item>
                                        <button onClick={() => props.callBack()} className="border-yellowColor border-[1px] w-full flex justify-center items-center px-10 h-[48px] font-medium text-[14px] text-yellowColor rounded-[11px] ">Précédent</button>
                                    </Form.Item>
                                    <Form.Item  >
                                        <button type='submit' onClick={() => handleSubmit()} className="bg-yellowColor flex w-full justify-center items-center px-10 h-[48px] font-medium text-[14px] text-background rounded-[11px] ">Confirmer</button>
                                    </Form.Item>
                                </div>
                            </Form>

                        </div>
                    </div> :
                        <div className=' flex justify-center p-5  items-start top-0 bottom-0 left-0 right-0'>
                            <div className="bg-background  space-y-5 flex flex-col justify-center items-center  w-[100%] md:w-[70%] lg:w-[60%] xl:w-[40%] rounded-[17px] p-5 ">
                                <div className='flex justify-center items-center space-x-5'>
                                    <img className='w-[40px] h-[40px] rounded-[17px]' src='/images/check.svg' />
                                    <h4 className="font-medium  text-greenColor text-[18px] lg:text-[22px] xl:text-[28px]">Demande envoyée</h4>
                                </div>
                                <p className="text-mouseGray text-center text-[20px] pb-[3rem] font-medium ">
                                    Nous avons bien reçu votre demande d’évaluation.
                                    Notre consultant vous contactera dans les prochains jours.
                                    Merci de vérifier également votre courriel.
                                </p>

                                <button onClick={() => router.back()} className='px-6 py-3 rounded-[16px] bg-yellowColor text-background font-medium text-[16px]'>Continuer</button>
                            </div>
                        </div>
                }

            </div >
        </>
    );
}