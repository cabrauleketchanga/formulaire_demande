interface Check {
    title: string;
    status: boolean;
    onChange: Function
}
export default function CheckBox(check: Check) {
    return (
        <>
            <div onClick={() => check.onChange()} className='flex cursor-pointer justify-start items-center space-x-2'>
                <div className='h-[20px] w-[20px]  cursor-pointer flex justify-center items-center border-mouseGray border-[1px] rounded-[20px]'>
                    {
                        check.status == true && (<div className='bg-mouseGray h-[15px] w-[15px] rounded-[18px]'></div>
                        )}
                </div>
                <h4 className='text-mouseGray text-[14px] pb-1 font-regular'>{check.title}</h4>

            </div>
        </>
    );
}