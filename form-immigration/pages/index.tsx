import Link from "next/link";

export default function Home() {
  return (
    <main className="transition-all duration-700 space-y-10  h-screen w-screen p-0 md:p-[4rem] bg-background ">
      <div className="space-y-10 p-4">
        <h3 className="font-semiBold flex justify-center items-center text-black text-md md:text-[22px] lg:text-[32px]">Demande de contact pour évaluation</h3>
        <div className="h-auto rounded-lg flex justify-center p-5  items-center w-full bg-no-repeat bg-cover bg-center bg-[url('/images/canada.png')]">
          <div className="bg-background flex flex-col justify-center items-center w-full lg:w-[40%] rounded-[17px] p-5 ">
            <h4 className="font-medium pb-10 text-greenColor text-md md:text-[20px] lg:text-[28px]">Formulaire d’immigration</h4>
            <p className="text-mouseGray text-[12px] pb-[3rem] font-medium ">ture MàJ pour améliorer organisation des torrents (images de couverture/TMDB) :Se joindre à la discussion sur le forum !
              CHANGEMENT DOMAINE 10/07/2023:Nous avons perdu le contrôle du nom de domaine yggtorrent.do (l&apos;enlever des favoris),</p>

            <p className="text-mouseGray text-[12px] pb-[3rem] font-medium ">
              notre nouveau nom de domaine est yggtorrent.wtfSuivez-nous sur les réseaux sociaux:Telegram: CLIQUER ICITwitter: CLIQUER ICIMastodon: CLIQUER ICIChangement WIKI  Ebooks (les doublons)Si vous n&apos;êtes pas adepte du téléchargement torrent et que vous débutez, Yggland (accessible ici)
            </p>

            <p className="text-mouseGray text-[12px] pb-[3rem] font-medium ">
              vous propose des ressources qui vous permettront d&apos;y voir plus clair et de vous aiguiller - notamment sur le choix des clients Torrent. Les Teams internes infos ici - Liste iciListe des exclusivités du moment - Liste iciAmis uploaders, prière de vous informer sur les modifs du WIK
            </p>

            <Link href="/registration">
              <button className="bg-yellowColor flex justify-center items-center px-10 h-[54px] font-medium text-[14px] text-background rounded-[12px] mt-10">Enregistrez-vous</button>

            </Link>
          </div>
        </div>
      </div>
    </main>
  )
}
